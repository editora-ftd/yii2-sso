<?php

namespace editoraftd\sso;

use Yii;

class Sso extends \yii\base\Component
{
    public $url;

    private $key;

    public function init()
    {
        if (!isset($_COOKIE['FTDSSO']) || !$this->checkKey()) {
            //clear old session
            Yii::$app->session->removeAll();
            $this->apiRedirect();
        }

        // Checa se a sessao é válida
        $cookie = json_decode($_COOKIE['FTDSSO']);

        if (!$this->hasKey($cookie->{'key'}))
            $this->setKey($cookie->{'key'});

        parent::init();
    }

    private function checkKey()
    {
        // Monta endereço de checagem por cUrl
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->getUrl() . "api/keycheck/" . $this->getKey());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $chk = curl_exec($ch);
        curl_close($ch);

        return $chk;
    }

    private function apiRedirect()
    {
        $red = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        $red = base64_encode($red);
        $red = $this->getUrl() . "?red=" . $red;
        header("location:$red");
        exit;
    }

    /**
     * @param $key
     * @return bool
     */
    public function hasKey($key)
    {
        return $this->getKey() == $key;
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }
}
