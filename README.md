YII2 SSO
========

Componente para realizar login dos usuários nas aplicações webs.

#### Configuração

    'sso' => [
        'url' => 'http://login.ftd.com.br/',
        'class' => 'vendor\path\to\Sso'
    ],